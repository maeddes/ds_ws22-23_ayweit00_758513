package com.example.firstapi;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class MatthiasTest {

    @Autowired
	private MockMvc mockMvc;

	@BeforeEach
	public void setup(){
		mockMvc = MockMvcBuilders.standaloneSetup(ApiController.class).build();
	}

	@Test
	public void testLittleSpook() throws Exception {
	  MockHttpServletResponse response = mockMvc.perform(MockMvcRequestBuilders.get("/todos/version")).andReturn().getResponse();
	  assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
	  assertThat(response.getContentAsString()).isEqualTo("v2.4");
	}

	@Test
	public void givenOne_whenGet_thenStatus200() throws Exception {
		// given
		TodoItem test1 = new TodoItem("item1");

		// when
		mockMvc.perform(get("/")
				.contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				// then
				.andExpect(jsonPath("$.todo").value("item1"));
	}

    
}
