package com.example.firstapi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

// CRUD Controller class
@RestController
@RequestMapping("/todos")
public class ApiController {

    private ArrayList<TodoItem> items = new ArrayList<>();

    public void startUp() {
        TodoItem item1 = new TodoItem("item1");
        TodoItem item2 = new TodoItem("item2");
        TodoItem item3 = new TodoItem("item3", 4);
        TodoItem item4 = new TodoItem("item4", 8);
        items.add(item1);
        items.add(item2);
        items.add(item3);
        items.add(item4);
    }

    @Operation(summary = "Returns the current API version")
    @GetMapping("/version")
    public String getVersion() {
        startUp();
        return "v2.4";
    }

    // POST (using path variables)
    @Operation(summary = "Creates a ToDo Item with path var and default priority of 2")
    @PostMapping("/{name}")
    public ResponseEntity<String> createAndAddItem(@PathVariable String name) {

        TodoItem newItem = new TodoItem(name);
        Boolean found = false;

        for (TodoItem item : items) {

            if (item.equals(newItem)) {
                found = true;
                break;
            }
        }

        if (!found) {
            items.add(newItem);
            return ResponseEntity.status(HttpStatus.CREATED).body("New item has been created");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Item already existed");
        }
    }

    // POST (using JSON object)
    @Operation(summary = "Creates a ToDo Item with a JSON object as request parameter")
    @PostMapping("/")
    public ResponseEntity<String> addItem(@RequestBody TodoItem newItem) {

        Boolean found = false;

        for (TodoItem item : items) {

            if (item.equals(newItem)) {
                found = true;
                break;
            }
        }

        if (!found) {
            items.add(newItem);
            return ResponseEntity.status(HttpStatus.CREATED).body("New item has been created");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Item already existed");
        }
    }

    // GET all
    @Operation(summary = "Returns a list of all ToDo items")
    @GetMapping(value = "/", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List has been delivered", content = @Content)
    })
    public List<TodoItem> getItems() {
        if (!items.isEmpty()) {
            return items;
        } else {
            return null;
        }
    }

    // GET one
    @Operation(summary = "Find a ToDo item by its itemId")
    @GetMapping(produces = "application/json", path = "/{itemId}")
    // @ResponseStatus(HttpStatus.OK)
    public Optional<TodoItem> getTodoItem(@PathVariable String itemId) {

        Optional<TodoItem> returnItem = Optional.empty();
        boolean found = false;

        for (TodoItem item : items) {

            if (item.getTodo().equals(itemId)) {
                returnItem = Optional.of(item);
                found = true;
            }
        }
        // TODO: Return 404 in case of item not found

        if (!found) {
            throw new ItemNotFoundException();
        }

        return returnItem;
    }

    // DELETE
    @Operation(summary = "Deletes a ToDo item, based on its name")
    @DeleteMapping(produces = "application/json", path = "/{itemId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodoItem(@PathVariable String itemId) {

        TodoItem tempItem = new TodoItem(itemId);
        Iterator<TodoItem> iterator = items.iterator();

        while (iterator.hasNext()) {
            if (iterator.next().equals(tempItem))
                iterator.remove();
        }
    }

    // DELETE
    @Operation(summary = "Deletes a ToDo item")
    @DeleteMapping(consumes = "application/json", produces = "application/json", path = "/")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "ToDo Item has been deleted", content = @Content)
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteItem(@RequestBody TodoItem item) {

        items.remove(item);
    }

    // PUT
    @Operation(summary = "Changes the priority of a ToDo item")
    @PutMapping(consumes = "application/json", produces = "application/json", path = "/")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "ToDo item has been updated", content = @Content)
    })
    @ResponseStatus(HttpStatus.OK)
    public TodoItem updateTodo(@RequestBody TodoItem todoItem) {

        int count = 0;
        TodoItem tmpItem;

        for (TodoItem item : items) {
            if (item.getTodo().equals(todoItem.getTodo())) {
                tmpItem = new TodoItem(item.getTodo(), todoItem.getPriority());
                items.set(count, tmpItem);
                return tmpItem;
            }
            count++;
        }

        return null;
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Item is not in the list.")
    class ItemNotFoundException extends RuntimeException {
    }

}
